const getAllUser = async () => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata").collection("user").find().toArray();
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const getUserByPayload = async (payload) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata").collection("user").findOne(payload);
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const addUser = async (newUser) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata").collection("user").insertOne(newUser);
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const udateUser = async (id, user) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata")
      .collection("user")
      .updateOne({ _id: id }, { $set: { ...user, updated_at: new Date() } });
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const deleteUser = async (id) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata")
      .collection("user")
      .deleteOne({ _id: id });
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

module.exports = {
  getAllUser,
  deleteUser,
  addUser,
  getUserByPayload,
  udateUser,
};
