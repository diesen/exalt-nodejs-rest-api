const express = require("express");
const {
  getAllPlace,
  addPlace,
  deletePlace,
  getPlaceByPayload,
  udatePlace,
} = require("../db/place");
const { getUserByPayload } = require("../db/user");
const { getPassByPayload } = require("../db/pass");
const {
  placeValidate,
  createPlaceValidate,
} = require("../validator/place.validator");
const { mongoIdValidate } = require("../validator/mongId.validator");
const parseJoiErrorHelper = require("../helper/parse-joi-error.helper");
const mongo = require("mongodb");
const { checkToken } = require("../middleware/check-token");

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Place
 *   description: Place management
 */

/**
 * @swagger
 * /place:
 *   post:
 *     tags:
 *       - Place
 *     summary: Create a new place
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/AddPlace'
 *     responses:
 *       201:
 *         description: successful
 *       400:
 *         description: Error in the body
 *       404:
 *         description: Pass not found
 *       401:
 *         description: Access denied. No token provided.
 *       500:
 *         description: Server error
 */
router.post("/", async (req, res) => {
  const { error } = createPlaceValidate(req.body);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }

  const pass = await getPassByPayload({
    level: req.body.minimumRequiredPassLevel,
  });
  if (!pass) {
    return res
      .status(404)
      .send("The Minimum required pass's level was not found");
  }
  const addedPlace = await addPlace(req.body);
  const result = await getPlaceByPayload({ _id: addedPlace.insertedId });
  res.status(201).json(result);
});

/**
 * @swagger
 * /place:
 *   put:
 *     tags:
 *       - Place
 *     summary: Update a place
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Place'
 *     responses:
 *       200:
 *         description: successful
 *       400:
 *         description: Error in the body
 *       404:
 *         description: Pass not found
 *       401:
 *         description: Access denied. No token provided.
 *       500:
 *         description: Server error
 */
router.put("/", async (req, res) => {
  const { error } = placeValidate(req.body);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }

  const id = mongo.ObjectId(req.body._id);
  const place = await getPlaceByPayload({ _id: id });
  if (!place) {
    return res.status(404).send("Place not found");
  }
  const pass = await getPassByPayload({
    level: req.body.minimumRequiredPassLevel,
  });
  if (!pass) {
    return res
      .status(404)
      .send("The Minimum required pass's level was not found");
  }
  req.body._id = id;
  await udatePlace(id, req.body);
  const result = await getPlaceByPayload({ _id: id });
  res.status(201).json(result);
});

/**
 * @swagger
 * /place:
 *   get:
 *     tags:
 *       - Place
 *     summary: Recieve list of place
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Succesful
 *         schema:
 *           $ref: '#/definitions/Place'
 *       401:
 *         description: Access denied. No token provided.
 *       500:
 *         description: Server error
 */
router.get("/", async (req, res) => {
  const result = await getAllPlace();
  res.status(200).json(result);
});

/**
 * @swagger
 * /place/{id}:
 *   get:
 *     tags:
 *       - Place
 *     summary: Recieve a place with a givin id
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: id of the place
 *         schema:
 *           type: string
 *         required:
 *           - id
 *     responses:
 *       200:
 *         description: Succesful
 *         schema:
 *           $ref: '#/definitions/Place'
 *       401:
 *         description: Access denied. No token provided.
 *       404:
 *        description: Place not found
 *       500:
 *         description: Server error
 */
router.get("/:id", async (req, res) => {
  const { error } = mongoIdValidate(req.params);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }
  const id = mongo.ObjectId(req.params.id);
  const result = await getPlaceByPayload({ _id: id });
  if (!result) {
    return res.status(404).send("Place not found");
  }
  res.status(200).json(result);
});

/**
 * @swagger
 * /place/can-access/{id}:
 *   get:
 *     tags:
 *       - Place
 *     summary: Check if the current user can access a given public place
 *     consumes:
 *       - application/json
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: id of the place
 *         schema:
 *           type: string
 *         required:
 *           - id
 *     responses:
 *       200:
 *         description: Succesful
 *         schema:
 *           $ref: '#/definitions/Place'
 *       401:
 *         description: Access denied. No token provided.
 *       403:
 *         description: Forbiden.
 *       404:
 *        description: Place not found
 *       500:
 *         description: Server error
 */
router.get("/can-access/:id", [checkToken], async (req, res) => {
  const { error } = mongoIdValidate(req.params);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }
  const id = mongo.ObjectId(req.params.id);
  const place = await getPlaceByPayload({ _id: id });
  if (!place) {
    return res.status(404).send("Place was not found");
  }
  const userId = mongo.ObjectId(req.user.userId);
  const user = await getUserByPayload({
    _id: userId,
  });
  if (!user) {
    return res.status(404).send("User was not found");
  }

  const userPass = await getPassByPayload({ _id: user.passId });
  if (!userPass) {
    return res.status(404).send("User pass was not found");
  }

  if (
    user.age < place.minimumRequiredAge ||
    userPass.level < place.minimumRequiredPassLevel
  ) {
    return res.status(403).send("User can't access");
  }

  res.status(200).json(place);
});

/**
 * @swagger
 * /place/{id}:
 *   delete:
 *     tags:
 *       - Place
 *     summary: delete a place
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: id of the place
 *         schema:
 *           type: string
 *         required:
 *           - id
 *     responses:
 *       200:
 *         description: successfully
 *       401:
 *         description: Access denied. No token provided.
 *       500:
 *         description: Server error
 */
router.delete("/:id", async (req, res) => {
  const { error } = mongoIdValidate(req.params);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }

  const id = mongo.ObjectId(req.params.id);
  const deletedPlace = await deletePlace(id);
  res.status(200).json(deletedPlace);
});

/**
 * @swagger
 * definitions:
 *  Place:
 *    properties:
 *      id:
 *        type: string
 *      Address:
 *        type: string
 *      phoneNumber:
 *        type: string
 *      minimumRequiredPassLevel:
 *        type: integer
 *      minimumRequiredAge:
 *        type: integer
 */

/**
 * @swagger
 * definitions:
 *  AddPlace:
 *    properties:
 *      Address:
 *        type: string
 *      phoneNumber:
 *        type: string
 *      minimumRequiredPassLevel:
 *        type: integer
 *      minimumRequiredAge:
 *        type: integer
 */

module.exports = router;
