const express = require("express");
const {
  getAllPass,
  addPass,
  deletePass,
  getPassByPayload,
  udatePass,
} = require("../db/pass");
const mongo = require("mongodb");
const { checkToken } = require("../middleware/check-token");
const {
  passValidate,
  createPassValidate,
} = require("../validator/pass.validator");
const { mongoIdValidate } = require("../validator/mongId.validator");
const parseJoiErrorHelper = require("../helper/parse-joi-error.helper");

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Pass
 *   description: Pass management
 */

/**
 * @swagger
 * /pass:
 *   post:
 *     tags:
 *       - Pass
 *     summary: Create a new pass
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/AddPass'
 *     responses:
 *       201:
 *         description: successful
 *         schema:
 *           $ref: '#/definitions/Pass'
 *       400:
 *         description: Error in the body
 *       401:
 *         description: Access denied. No token provided.
 *       409:
 *         description: Ressource already exist.
 *       500:
 *         description: Server error
 */
router.post("/", [checkToken], async (req, res) => {
  const { error } = createPassValidate(req.body);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }

  const pass = await getPassByPayload({ level: req.body.level });
  if (pass) {
    return res.status(409).send("Pass with the given level already exist");
  }
  const newPass = {
    label: req.body.label,
    level: req.body.level,
    created_at: new Date(),
    updated_at: new Date(),
  };
  const addedPass = await addPass(newPass);
  const result = await getPassByPayload({ _id: addedPass.insertedId });
  res.status(201).json(result);
});

/**
 * @swagger
 * /pass:
 *   put:
 *     tags:
 *       - Pass
 *     summary: Update a pass
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Pass'
 *     responses:
 *       201:
 *         description: successful
 *         schema:
 *           $ref: '#/definitions/Pass'
 *       400:
 *         description: Error in the body
 *       401:
 *         description: Access denied. No token provided.
 *       500:
 *         description: Server error
 */
router.put("/", [checkToken], async (req, res) => {
  const { error } = passValidate(req.body);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }

  const id = mongo.ObjectId(req.body._id);
  const pass = await getPassByPayload({ _id: id });
  if (!pass) {
    return res.status(404).send("Pass was not found");
  }
  req.body._id = id;
  await udatePass(id, req.body);
  const result = await getPassByPayload({ _id: id });
  res.status(201).json(result);
});

/**
 * @swagger
 * /pass:
 *   get:
 *     tags:
 *       - Pass
 *     summary: Recieve list of pass
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Succesful
 *         schema:
 *           $ref: '#/definitions/Pass'
 *       401:
 *         description: Access denied. No token provided.
 *       500:
 *         description: Server error
 */
router.get("/", async (req, res) => {
  const result = await getAllPass();
  res.status(200).json(result);
});

/**
 * @swagger
 * /pass/{id}:
 *   get:
 *     tags:
 *       - Pass
 *     summary: Recieve a pass with a givin id
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: id of the pass
 *         schema:
 *           type: string
 *         required:
 *           - id
 *     responses:
 *       200:
 *         description: Succesful
 *         schema:
 *           $ref: '#/definitions/Pass'
 *       401:
 *         description: Access denied. No token provided.
 *       404:
 *        description: Pass not found
 *       500:
 *         description: Server error
 */
router.get("/:id", [checkToken], async (req, res) => {
  const { error } = mongoIdValidate(req.params);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }
  const id = mongo.ObjectId(req.params.id);
  const result = await getPassByPayload({ _id: id });
  if (!result) {
    return res.status(404).send("Pass not found");
  }
  res.status(200).json(result);
});

/**
 * @swagger
 * /pass/{id}:
 *   delete:
 *     tags:
 *       - Pass
 *     summary: delete a pass
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: id of the pass
 *         schema:
 *           type: string
 *         required:
 *           - id
 *     responses:
 *       200:
 *         description: successfully
 *       401:
 *         description: Access denied. No token provided.
 *       500:
 *         description: Server error
 */
router.delete("/:id", [checkToken], async (req, res) => {
  const { error } = mongoIdValidate(req.params);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }
  const id = mongo.ObjectId(req.params.id);
  const deletedPass = await deletePass(id);
  res.status(200).json(deletedPass);
});

/**
 * @swagger
 * definitions:
 *  Pass:
 *    properties:
 *      id:
 *        type: string
 *      label:
 *        type: string
 *      level:
 *        type: integer
 *      created_at:
 *        type: date
 *      updated_at:
 *        type: date
 */

/**
 * @swagger
 * definitions:
 *  AddPass:
 *    properties:
 *      label:
 *        type: string
 *      level:
 *        type: integer
 */

module.exports = router;
