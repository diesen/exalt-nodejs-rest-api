const express = require("express");
const bodyParser = require("body-parser");
const pass = require("../route/pass");
const place = require("../route/place");
const user = require("../route/user");

module.exports = function (app) {
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(express.json());

  app.use("/api/pass", pass);
  app.use("/api/place", place);
  app.use("/api/user", user);
  app.use(function (err, req, res, next) {
    res.status(500).send("Something failed.");
  });
};
