const express = require("express");
const { MongoClient } = require("mongodb");

const app = express();

if (!process.env.MONGODB_CONNSTRING) {
  throw new Error("Missing env var MONGODB_CONNSTRING");
}

if (!process.env.TOKEN_STRING) {
  throw new Error("Missing env var TOKEN_STRING");
}
global.TOKEN_STRING = process.env.TOKEN_STRING;
global.CLIENT = new MongoClient(process.env.MONGODB_CONNSTRING);
require("./initialize/header")(app);
require("./initialize/route")(app);
require("./initialize/swagger")(app);
require("./initialize/init-data")();

const port = process.env.PORT || 8080;
const server = app.listen(port, async () => {
  console.info(`Listening on ports ${port}...`);
});

module.exports = server;
