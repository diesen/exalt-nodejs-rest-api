module.exports = async function (details) {
  var dataError = {};
  for await (detail of details) {
    var key = detail.path[0];
    if (!dataError[key]) dataError[key] = [];
    dataError[key].push(detail.message);
  }
  return dataError;
};
