const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const mongoIdSchema = Joi.object({
  id: Joi.objectId().required(),
});

function mongoIdValidate(param) {
  return mongoIdSchema.validate(param, { abortEarly: false });
}

module.exports = { mongoIdValidate };
