const express = require("express");
const {
  addUser,
  deleteUser,
  getUserByPayload,
  udateUser,
} = require("../db/user");
const { getPassByPayload } = require("../db/pass");
const { getAllPlaceByPayload } = require("../db/place");
const mongo = require("mongodb");
const { checkToken } = require("../middleware/check-token");
const {
  userValidate,
  createUserValidate,
  userAccesValidate,
} = require("../validator/user.validator");
const { mongoIdValidate } = require("../validator/mongId.validator");
const parseJoiErrorHelper = require("../helper/parse-joi-error.helper");
const jwt = require("jsonwebtoken");

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: User
 *   description: User management
 */

function getToken(user) {
  return jwt.sign(
    {
      userId: user._id,
    },
    global.TOKEN_STRING
  );
}

/**
 * @swagger
 * /User:
 *   post:
 *     tags:
 *       - User
 *     summary: Create a new User
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/AddUser'
 *     responses:
 *       201:
 *         description: successful
 *         schema:
 *           $ref: '#/definitions/User'
 *       400:
 *         description: Error in the body
 *       409:
 *         description: Ressource already exist.
 *       500:
 *         description: Server error
 */
router.post("/", async (req, res) => {
  const { error } = createUserValidate(req.body);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }

  const user = await getUserByPayload({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
  });
  if (user) {
    return res.status(409).send("User already exist");
  }

  const passId = mongo.ObjectId(req.body.passId);
  const pass = await getPassByPayload({ _id: passId });
  if (!pass) {
    return res.status(404).send("Given pass was not found");
  }

  const newUser = {
    ...req.body,
    passId,
    created_at: new Date(),
    updated_at: new Date(),
  };
  const addedUser = await addUser(newUser);
  const result = await getUserByPayload({ _id: addedUser.insertedId });
  const token = getToken(result);
  res
    .status(201)
    .header("x-auth-token", token)
    .header("access-control-expose-headers", "x-auth-token")
    .json(result);
});

/**
 * @swagger
 * /User/access:
 *   post:
 *     tags:
 *       - User
 *     summary: Get User access
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/UserAccess'
 *     responses:
 *       201:
 *         description: successful
 *         schema:
 *           $ref: '#/definitions/User'
 *       400:
 *         description: Error in the body
 *       500:
 *         description: Server error
 */
router.post("/access", async (req, res) => {
  const { error } = userAccesValidate(req.body);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }

  const user = await getUserByPayload({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
  });

  if (!user) {
    return res.status(404).send("User was not found");
  }

  const token = getToken(user);
  res
    .status(201)
    .header("x-auth-token", token)
    .header("access-control-expose-headers", "x-auth-token")
    .json(user);
});

/**
 * @swagger
 * /user:
 *   put:
 *     tags:
 *       - User
 *     summary: Update a user
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       201:
 *         description: successful
 *         schema:
 *           $ref: '#/definitions/User'
 *       400:
 *         description: Error in the body
 *       401:
 *         description: Access denied. No token provided.
 *       403:
 *         description: Forbiden.
 *       500:
 *         description: Server error
 */
router.put("/", [checkToken], async (req, res) => {
  const { error } = userValidate(req.body);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }

  if (req.user.userId !== req.body._id) {
    return res.status(403).send("Forbiden");
  }
  const userId = mongo.ObjectId(req.body._id);
  const user = await getUserByPayload({
    _id: userId,
  });
  if (!user) {
    return res.status(404).send("User was not found");
  }

  const passId = mongo.ObjectId(req.body.passId);
  const pass = await getPassByPayload({ _id: passId });
  if (!pass) {
    return res.status(404).send("Given pass was not found");
  }

  req.body._id = userId;
  await udateUser(userId, req.body);
  const result = await getUserByPayload({ _id: userId });
  const token = getToken(result);
  res
    .status(201)
    .header("x-auth-token", token)
    .header("access-control-expose-headers", "x-auth-token")
    .json(result);
});

/**
 * @swagger
 * /user/access-place:
 *   get:
 *     tags:
 *       - User
 *     summary: Get all places user can access
 *     consumes:
 *       - application/json
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Succesful
 *         schema:
 *           $ref: '#/definitions/Place'
 *       401:
 *         description: Access denied. No token provided.
 *       404:
 *        description: ressource not found
 *       500:
 *         description: Server error
 */
router.get("/access-place", [checkToken], async (req, res) => {
  const userId = mongo.ObjectId(req.user.userId);
  const user = await getUserByPayload({
    _id: userId,
  });
  if (!user) {
    return res.status(404).send("User was not found");
  }
  const userPass = await getPassByPayload({ _id: user.passId });
  if (!userPass) {
    return res.status(404).send("User pass was not found");
  }

  const allPlace = await getAllPlaceByPayload({
    minimumRequiredPassLevel: { $gte: userPass.level },
    minimumRequiredAge: { $gte: user.age },
  });

  res.status(200).json(allPlace);
});

/**
 * @swagger
 * /user/{id}:
 *   get:
 *     tags:
 *       - User
 *     summary: Recieve a user with a givin id
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: id of the user
 *         schema:
 *           type: string
 *         required:
 *           - id
 *     responses:
 *       200:
 *         description: Succesful
 *         schema:
 *           $ref: '#/definitions/User'
 *       401:
 *         description: Access denied. No token provided.
 *       403:
 *         description: Forbiden.
 *       404:
 *        description: User not found
 *       500:
 *         description: Server error
 */
router.get("/:id", [checkToken], async (req, res) => {
  const { error } = mongoIdValidate(req.params);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }

  if (req.user.userId !== req.params.id) {
    return res.status(403).send("Forbiden");
  }

  const userId = mongo.ObjectId(req.params.id);
  const result = await getUserByPayload({ _id: userId });
  if (!result) {
    return res.status(404).send("User was not found");
  }
  const token = getToken(result);
  res
    .status(200)
    .header("x-auth-token", token)
    .header("access-control-expose-headers", "x-auth-token")
    .json(result);
});

/**
 * @swagger
 * /user/{id}:
 *   delete:
 *     tags:
 *       - User
 *     summary: delete a user
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: id of the User to delete
 *         schema:
 *           type: string
 *         required:
 *           - id
 *     responses:
 *       200:
 *         description: successfully
 *       401:
 *         description: Access denied. No token provided.
 *       403:
 *         description: Forbiden.
 *       500:
 *         description: Server error
 */
router.delete("/:id", [checkToken], async (req, res) => {
  const { error } = mongoIdValidate(req.params);
  if (error) {
    const dataError = await parseJoiErrorHelper(error.details);
    return res.status(400).send(dataError);
  }

  if (req.user.userId !== req.params.id) {
    return res.status(403).send("Forbiden");
  }

  const id = mongo.ObjectId(req.params.id);
  const deletedUser = await deleteUser(id);
  res.status(200).json(deletedUser);
});

/**
 * @swagger
 * definitions:
 *  User:
 *    properties:
 *      id:
 *        type: string
 *      firstName:
 *        type: string
 *      lastName:
 *        type: string
 *      age:
 *        type: integer
 *      phoneNumber:
 *        type: string
 *      Address:
 *        type: string
 *      passId:
 *        type: string
 *      created_at:
 *        type: date
 *      updated_at:
 *        type: date
 */

/**
 * @swagger
 * definitions:
 *  AddUser:
 *    properties:
 *      firstName:
 *        type: string
 *      lastName:
 *        type: string
 *      age:
 *        type: integer
 *      phoneNumber:
 *        type: string
 *      Address:
 *        type: string
 *      passId:
 *        type: string
 */

/**
 * @swagger
 * definitions:
 *  UserAccess:
 *    properties:
 *      firstName:
 *        type: string
 *      lastName:
 *        type: string
 */

module.exports = router;
