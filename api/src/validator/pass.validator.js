const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const passModelSchema = Joi.object({
  _id: Joi.objectId().required(),
  label: Joi.string().min(2).max(50).required(),
  level: Joi.number().required(),
  created_at: Joi.date().required(),
  updated_at: Joi.date().required(),
});

const createPassModelSchema = Joi.object({
  label: Joi.string().min(2).max(50).required(),
  level: Joi.number().required(),
});

function passValidate(body) {
  return passModelSchema.validate(body, { abortEarly: false });
}

function createPassValidate(body) {
  return createPassModelSchema.validate(body, { abortEarly: false });
}

module.exports = { passValidate, createPassValidate };
