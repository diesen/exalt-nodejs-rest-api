const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const placeModelSchema = Joi.object({
  _id: Joi.objectId().required(),
  Address: Joi.string().min(2).max(50).required(),
  phoneNumber: Joi.string().min(10).max(15).required(),
  minimumRequiredPassLevel: Joi.number().required(),
  minimumRequiredAge: Joi.number().required(),
});

const createPlaceModelSchema = Joi.object({
  Address: Joi.string().min(2).max(50).required(),
  phoneNumber: Joi.string().min(10).max(15).required(),
  minimumRequiredPassLevel: Joi.number().required(),
  minimumRequiredAge: Joi.number().required(),
});

function placeValidate(body) {
  return placeModelSchema.validate(body, { abortEarly: false });
}

function createPlaceValidate(body) {
  return createPlaceModelSchema.validate(body, { abortEarly: false });
}

module.exports = { placeValidate, createPlaceValidate };
