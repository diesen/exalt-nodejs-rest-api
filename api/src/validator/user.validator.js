const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const userModelSchema = Joi.object({
  _id: Joi.objectId().required(),
  firstName: Joi.string().min(2).max(50).required(),
  lastName: Joi.string().min(2).max(50).required(),
  phoneNumber: Joi.string().min(10).max(15).required(),
  age: Joi.number().required(),
  Address: Joi.string().min(2).max(50).required(),
  passId: Joi.objectId().required(),
  created_at: Joi.date().required(),
  updated_at: Joi.date().required(),
});

const userAccessModelSchema = Joi.object({
  firstName: Joi.string().min(2).max(50).required(),
  lastName: Joi.string().min(2).max(50).required(),
});

const createUserModelSchema = Joi.object({
  firstName: Joi.string().min(2).max(50).required(),
  lastName: Joi.string().min(2).max(50).required(),
  phoneNumber: Joi.string().min(10).max(15).required(),
  age: Joi.number().required(),
  Address: Joi.string().min(2).max(50).required(),
  passId: Joi.objectId().required(),
});

function userValidate(body) {
  return userModelSchema.validate(body, { abortEarly: false });
}

function userAccesValidate(body) {
  return userAccessModelSchema.validate(body, { abortEarly: false });
}

function createUserValidate(body) {
  return createUserModelSchema.validate(body, { abortEarly: false });
}

module.exports = { userValidate, createUserValidate, userAccesValidate };
