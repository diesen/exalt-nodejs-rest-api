const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

module.exports = function (app) {
  const swaggerDefinition = {
    info: {
      title: "Exalt kata",
      version: "1.0.0",
      description: "Exalt kata nodejs - rest api",
    },
    host: "localhost:8080",
    basePath: "/api",
    securityDefinitions: {
      bearerAuth: {
        type: "apiKey",
        name: "x-auth-token",
        in: "header",
      },
    },
  };
  const swaggerOptions = {
    swaggerDefinition,
    apis: ["./src/route/*.js"],
  };

  const swaggerDocs = swaggerJsDoc(swaggerOptions);
  app.use("/", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
};
