const passData = require("../defaut-db-data/pass-data.json");
const placeData = require("../defaut-db-data/place-data.json");

module.exports = async function () {
  try {
    await global.CLIENT.connect();
    await global.CLIENT.db("kata").collection("pass").insertMany(passData.data);
    await global.CLIENT.db("kata")
      .collection("place")
      .insertMany(placeData.data);
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};
