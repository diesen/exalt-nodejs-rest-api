const getAllPlace = async () => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata").collection("place").find().toArray();
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const getPlaceByPayload = async (payload) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata").collection("place").findOne(payload);
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const getAllPlaceByPayload = async (payload) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata")
      .collection("place")
      .find(payload)
      .toArray();
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const addPlace = async (newPlace) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata")
      .collection("place")
      .insertOne(newPlace);
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const udatePlace = async (id, place) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata")
      .collection("place")
      .updateOne({ _id: id }, { $set: place });
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const deletePlace = async (id) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata")
      .collection("place")
      .deleteOne({ _id: id });
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

module.exports = {
  getAllPlace,
  deletePlace,
  addPlace,
  getPlaceByPayload,
  udatePlace,
  getAllPlaceByPayload,
};
