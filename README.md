# exalt - nodejs - rest api

## Provider

Before run the project you need to install Docker https://docs.docker.com/get-docker/

## Getting start

To Run the project you must execute the following commands at the root of the repository

```
cd api
docker compose build --build-arg NODE_VERSION=$(cat .nvmrc | tr -cd [:digit:].) --build-arg TOKEN_STRING='azertyu§(erghjhgfd)'
docker compose up
```
