const getAllPass = async () => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata").collection("pass").find().toArray();
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const getPassByPayload = async (payload) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata").collection("pass").findOne(payload);
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const addPass = async (newPass) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata").collection("pass").insertOne(newPass);
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const udatePass = async (id, pass) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata")
      .collection("pass")
      .updateOne({ _id: id }, { $set: { ...pass, updated_at: new Date() } });
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

const deletePass = async (id) => {
  try {
    await global.CLIENT.connect();
    return await global.CLIENT.db("kata")
      .collection("pass")
      .deleteOne({ _id: id });
  } catch (e) {
    throw new Error(e);
  } finally {
    await global.CLIENT.close();
  }
};

module.exports = {
  getAllPass,
  deletePass,
  addPass,
  getPassByPayload,
  udatePass,
};
